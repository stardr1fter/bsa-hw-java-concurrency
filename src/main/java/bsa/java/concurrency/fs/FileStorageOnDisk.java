package bsa.java.concurrency.fs;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.CompletableFuture;

@Component
@Slf4j
public class FileStorageOnDisk implements FileSystem {
    private final String WEB_ROOT_PATH = "src/main/resources/static";
    private final String WEB_ROOT_DIR_NAME = "static";

    private final Path baseDirPath;

    @SneakyThrows
    public FileStorageOnDisk() {
        var cannonical = new File(WEB_ROOT_PATH).getCanonicalPath();
        baseDirPath = Path.of(cannonical);
        log.info(String.format("Created file storage on disk at '%s'", baseDirPath));
    }

    // these should have probably been left blocking...
    @Override
    public CompletableFuture<String> saveFile(String path, byte[] file) {
        return CompletableFuture.supplyAsync(() -> {
            throwIfAbsolute(path);
            Path targetPath = null;
            try {
                targetPath = createAbsolutePath(path);
                log.info(String.format("Saving file to '%s'", targetPath));
                ensureDirectoryHierarchy(targetPath.getParent());
                Files.write(targetPath, file, StandardOpenOption.CREATE_NEW);
            } catch (IOException | SecurityException | InvalidPathException e) {
                log.error(String.format("Error saving file '%s'. Because: %s", targetPath, e.getMessage()));
                throw new FileSystemException("Error writing file to disk", e);
            }
            return trimToWebRoot(targetPath);
        });
    }

    @Override
    public CompletableFuture<Void> deleteFile(String path) {
        return CompletableFuture.runAsync(() -> {
            throwIfAbsolute(path);
            Path targetPath = null;
            try {
                targetPath = createAbsolutePath(path);
                log.info(String.format("Deleting file at '%s'", targetPath));
                Files.deleteIfExists(targetPath);
            } catch (IOException | SecurityException | InvalidPathException e) {
                log.error(String.format("Error saving file '%s'. Because: %s", targetPath, e.getMessage()));
            }
        });
    }

    @Override
    public CompletableFuture<Void> clear() {
        return CompletableFuture.runAsync(() -> {
            try {
                synchronized (this) {
                    FileSystemUtils.deleteRecursively(baseDirPath);
                }
            } catch (IOException e) {
                log.error(String.format(
                        "Error deleting all files recoursively under '%s'. Because: %s",
                        baseDirPath, e.getMessage()));
                throw new FileSystemException("Error clearing file storage on disk", e);
            }
        });
    }

    private void throwIfAbsolute(String path) {
        if (new File(path).isAbsolute()) {
            throw new FileSystemException("Got absolute path (expected relative).");
        }
    }

    private Path createAbsolutePath(String relativePath) {
        return Paths.get(baseDirPath.toString(), relativePath);
    }

    private String trimToWebRoot(Path path) {
        for (int i = 0; i < path.getNameCount(); i++) {
            var part = path.getName(i).toString();
            if (part.equals(WEB_ROOT_DIR_NAME)) {
                var fromWebRoot = path.subpath(i + 1, path.getNameCount());
                return fromWebRoot.toString().replace('\\', '/');
            }
        }
        throw new FileSystemException("Unable to locate web root.");
    }

    private synchronized void ensureDirectoryHierarchy(Path dirPath) throws IOException {
        Files.createDirectories(dirPath);
    }
}
