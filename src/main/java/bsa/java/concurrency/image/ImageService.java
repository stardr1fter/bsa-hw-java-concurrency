package bsa.java.concurrency.image;

import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.exceptions.ImageServiceException;
import bsa.java.concurrency.image.utils.DHasher;
import bsa.java.concurrency.image.utils.HashingUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.stream.Stream;

@Slf4j
@Service
public class ImageService {
    @Autowired
    private DHasher dHasher;// Images are saved at static/images/{uuid}.jpg on disk.

    @Autowired
    private FileSystem fs;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private ExecutorService executor;

    @SneakyThrows
    public List<SearchResultDTO> searchMatchesOrSave(byte[] imageFile, double threshold) {
        if (threshold <= 0 || threshold > 1) {
            throw new ImageServiceException("Threshold is not within acceptable range.");
        }

        long hash = dHasher.calculateHash(imageFile).get();

        // TODO: replace this with db query
        var images = imageRepository.findAll();
        var similarImages = new ArrayList<SearchResultDTO>();
        for (var image : images) {
            double diff = HashingUtils.calculateMatchPercent(hash, image.getDHash());
            if (diff >= threshold) {
                similarImages.add(new SearchResultDTO(image.getId(), diff, image.getPublicUrl()));
            }
        }

        if (similarImages.isEmpty()) {
            CompletableFuture.runAsync(() -> {
                log.info("No matches found. Processing the file.");
                var id = UUID.randomUUID();
                var saveFileTask = fs.saveFile(createImageLocalPath(id), imageFile);
                try {
                    Image newImg = Image.builder()
                            .id(UUID.randomUUID())
                            .dHash(hash)
                            .publicUrl(saveFileTask.get())
                            .build();
                    imageRepository.save(newImg);
                } catch (InterruptedException | ExecutionException e) {
                    log.error("Error during saving unique image", e);
                }
            }, executor);
        }

        return similarImages; // return response without waiting for file
    }

    public void batchUploadFiles(Stream<byte[]> imageFiles) {
        imageFiles
                .parallel()
                .map(imageFile -> {
                    var id = UUID.randomUUID();
                    var saveFileTask = fs.saveFile(createImageLocalPath(id), imageFile);
                    var calculateHashTask = dHasher.calculateHash(imageFile);

                    try {
                        return Image.builder()
                                .id(id)
                                .publicUrl(saveFileTask.get())
                                .dHash(calculateHashTask.get())
                                .build();
                    } catch (InterruptedException | ExecutionException e) {
                        throw new ImageServiceException("Error processing a batch of files: " + e.getMessage(), e);
                    }
                })
                .forEach(entity -> imageRepository.save(entity));
    }

    public void deleteImage(UUID id) {
        fs.deleteFile(createImageLocalPath(id));
        imageRepository.deleteById(id);
    }

    public void deleteAllImages() {
        fs.clear();
        imageRepository.deleteAll();
    }

    private String createImageLocalPath(UUID imageId) {
        return String.format("images/%s.jpg", imageId);
    }
}
