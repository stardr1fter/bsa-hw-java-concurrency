package bsa.java.concurrency.image.exceptions;

public class ImageServiceException extends RuntimeException {
    public ImageServiceException(String message) {
        super(message);
    }

    public ImageServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
