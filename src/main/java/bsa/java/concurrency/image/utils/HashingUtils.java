package bsa.java.concurrency.image.utils;

public class HashingUtils {
    public static double calculateMatchPercent(long hash1, long hash2) {
        double diffPercent = (double)Long.bitCount(hash1 ^ hash2) / 64;
        return 1 - diffPercent;
    }
}
