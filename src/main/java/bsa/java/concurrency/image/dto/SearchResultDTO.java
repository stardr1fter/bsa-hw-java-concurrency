package bsa.java.concurrency.image.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@AllArgsConstructor
@Data
public class SearchResultDTO {
    private UUID imageId;
    private Double matchPercent;
    private String imageUrl;
}

//public interface SearchResultDTO {
//    UUID getImageId();
//    Double getMatchPercent();
//    String getImageUrl();
//}
